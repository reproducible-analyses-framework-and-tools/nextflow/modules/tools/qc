#!/usr/bin/env nextflow

include { manifest_to_raw_fqs } from '../preproc/preproc.nf'
include { raw_fqs_to_procd_fqs } from '../preproc/preproc.nf'

include { seqtk_sample as seqtk_sample_dna } from '../seqtk/seqtk.nf'
include { seqtk_sample as seqtk_sample_rna } from '../seqtk/seqtk.nf'

include { fastqc } from '../fastqc/fastqc.nf'

include { kraken2 } from '../kraken2/kraken2.nf'

include { somalier_extract } from '../somalier/somalier.nf'
include { somalier_relate } from '../somalier/somalier.nf'

include { multiqc as multiqc_subd } from '../multiqc/multiqc.nf'
include { multiqc as multiqc_subd_by_pat } from '../multiqc/multiqc.nf'
include { picard_collect_rna_seq_metrics } from '../picard2/picard2.nf'
include { picard_collect_wgs_metrics_nzc } from '../picard2/picard2.nf'
include { picard_collect_vcf_metrics } from '../picard2/picard2.nf'
include { gtf_to_refflat } from '../ref_utils/ref_utils.nf'
include { gtf_to_rrna_intervals } from '../ref_utils/ref_utils.nf'
include { get_fastqs } from '../utilities/utilities.nf'
include { samtools_stats } from '../samtools/samtools.nf'

include { lenstools_consolidate_multiqc_stats } from '../lenstools/lenstools.nf'


workflow bams_to_rna_seq_metrics {
// require:
//   params.qc$bams_to_rna_seq_metrics$qc_genome
//   BAMS
//   params.qc$bams_to_rna_seq_metrics$gtf
//   params.qc$bams_to_rna_seq_metrics$picard_collect_rna_seq_metrics_parameters
//   params.qc$bams_to_rna_seq_metrics$picard_strand_specificity
  take:
    fa
    bams
    gtf
    picard_collect_rna_seq_metrics_parameters
    picard_strand_specificity
    samtools_faidx_parameters
  main:
    gtf_to_refflat(
      gtf)
    gtf_to_rrna_intervals(
      fa,
      gtf,
      samtools_faidx_parameters)
    picard_collect_rna_seq_metrics(
      bams,
      picard_collect_rna_seq_metrics_parameters,
      gtf_to_refflat.out.refflat,
      gtf_to_rrna_intervals.out.rrna_intervals,
      picard_strand_specificity)
  emit:
    rna_seq_metrics_reports = picard_collect_rna_seq_metrics.out.rna_seq_metrics_reports
    //rna_seq_metrics_pdfs = picard_collect_rna_seq_metrics.out.rna_seq_metrics_pdfs
}


workflow fastqs_to_fastqc {
// require:
//   FASTQS
//   params.qc$fastqs_to_fastqc$fastqc_parameters
  take:
    fastqs
    fastqc_parstr
  main:
    fastqc(
      fastqs,
      fastqc_parstr)
  emit:
    fastqc_reports = fastqc.out.fastqc_reports
}


workflow manifest_to_kraken2 {
// require:
//   MANIFEST
  take:
    manifest
  main:
    get_fastqs(
      manifest.map{ [it[0], it[1], it[2], it[3]] },
      params.fq_dir)
    raw_fqs_to_kraken2(
      get_fastqs.out.fastqs)
  emit:
    kraken_outs = raw_fqs_to_kraken2.out.kraken_outs
}


workflow procd_fqs_to_metagenomics {
// require:
//   FQS
  take:
    fqs
    metagenomic_tool
    metagenomic_ref
    metagenomic_parameters
  main:
    kraken2(
      fqs,
      metagenomic_ref,
      metagenomic_parameters)
//  emit:
//    kraken_outs = kraken2.out.kraken_outs
}


workflow filter_samples {
// require:
//   params.qc$filter_samp_ids$samps
//   params.qc$filter_samp_ids$qc_pass_files
  take:
    samps
    qc_pass_files
  main:
    filter_samples_sub(samps, qc_pass_files)
    filter_samples_sub.out.passes.map{ it -> [it[0], it[1], it[2], it[3]]}.set{ filtered_samps }
  emit:
    filtered_samps = filtered_samps
}


process filter_samples_sub {
// Strategy here is to filter samples by ensuring they are contained within all
// QC files, creating a flag file, and then not emitting that sample if the
// flag file doesn't exist.

  input:
  tuple val(pat_name), val(run), val(dataset), path(fle)
  path qc_pass_files

  output:
  tuple val(pat_name), val(run), val(dataset), path(fle), path('flag.file'), emit: passes optional true

  script:
  """
  QC_FILES_ACTUAL_COUNT=\$(ls *samples | wc -l)
  QC_FILES_DETECTED_COUNT=\$(grep ${run} *samples | wc -l)

  if [ \${QC_FILES_ACTUAL_COUNT} -eq \${QC_FILES_DETECTED_COUNT} ]; then
    touch 'flag.file'
  fi
  """
}


workflow bams_to_wgs_nzc_metrics {
// require:
//   ALNS
//   params.qc$bams_to_wgs_nzc_metrics$dna_ref
  take:
    alns
    fa
  main:
    picard_collect_wgs_metrics_nzc(
      alns,
      fa,
      params.qc$bams_to_wgs_nzc_metrics$collect_wgs_nzc_parameters)
//  emit:
}


workflow vcfs_to_vcf_metrics {
// require:
//   VCFS
//   params.qc$vcfs_to_vcf_metrics$dbsnp_ref
  take:
    vcfs
    dbsnp
    picard_collect_vcf_metrics_parameters
  main:
    picard_collect_vcf_metrics(
      vcfs,
      dbsnp,
      picard_collect_vcf_metrics_parameters)
  emit:
    detail_metrics = picard_collect_vcf_metrics.out.detail_metrics
    summary_metrics = picard_collect_vcf_metrics.out.summary_metrics
}


workflow manifest_to_initial_qc {
// require:
//   MANIFEST
//   params.qc$manifest_to_initial_qc$fq_trim_tool
//   params.qc$manifest_to_initial_qc$fq_trim_tool_parameters
//   params.qc$manifest_to_initial_qc$metagenomic_tool
//   params.qc$manifest_to_initial_qc$metagenomic_ref
//   params.qc$manifest_to_initial_qc$metagenomic_parameters
  take:
    manifest
    fq_trim_tool
    fq_trim_tool_parameters
    metagenomic_tool
    metagenomic_ref
    metagenomic_parameters
  main:
    get_fastqs(
      manifest.map{ [it[0], it[1], it[2], it[3]] },
      params.fq_dir)

    manifest_to_raw_fqs(
      manifest)

    raw_fqs_to_initial_qc(
      manifest_to_raw_fqs.out.fqs,
      fq_trim_tool,
      fq_trim_tool_parameters,
      metagenomic_tool,
      metagenomic_ref,
      metagenomic_parameters)
}

workflow raw_fqs_to_initial_qc {
// require:
// FQS
  take:
    fqs
    fq_trim_tool
    fq_trim_tool_parameters
    metagenomic_tool
    metagenomic_ref
    metagenomic_parameters
  main:
//    fqs_to_sub_fqs_dna(
//      fastqs.filter{ it[1] =~ /^ad-|^nd-/ },
//      subsamp_tool,
//      subsamp_parameters)
//    fqs_to_sub_fqs_rna(
//      fastqs.filter{ it[1] =~ /^ar-|^nr-/ },
//      subsamp_tool,
//      subsamp_parameters)
    raw_fqs_to_procd_fqs(
      fqs,
      fq_trim_tool,
      fq_trim_tool_parameters)
    procd_fqs_to_initial_qc(
      raw_fqs_to_procd_fqs.out.procd_fqs,
      metagenomic_tool,
      metagenomic_ref,
      metagenomic_parameters)
}

workflow procd_fqs_to_initial_qc {
// require:
//   PROCD_FQS
  take:
    procd_fqs
    metagenomic_tool
    metagenomic_ref
    metagenomic_parameters
  main:
    seqtk_sample_dna(
      procd_fqs.filter{ it[1] =~ /^ad-|^nd-/ },
      params.qc$filter_samps_by_qc$seqtk_sample_count,
      params.qc$filter_samps_by_qc$seqtk_sample_seed,
      params.qc$filter_samps_by_qc$seqtk_sample_suffix,
      params.qc$filter_samps_by_qc$seqtk_sample_parameters)
    seqtk_sample_rna(
      procd_fqs.filter{ it[1] =~ /^ar-|^nr-/ },
      params.qc$filter_samps_by_qc$seqtk_sample_count,
      params.qc$filter_samps_by_qc$seqtk_sample_seed,
      params.qc$filter_samps_by_qc$seqtk_sample_suffix,
      params.qc$filter_samps_by_qc$seqtk_sample_parameters)
    procd_fqs_to_metagenomics(
      procd_fqs,
      '',
      metagenomic_ref,
      metagenomic_parameters)
}

workflow alns_to_sample_swap_check {
// Removing require block for now. Seems to be getting captured into main.nf
// require:
  take:
    bams
    manifest
    sample_swap_tool
    dna_ref
    known_sites
    sample_swap_tool_parameters
  main:
    sample_swap_tool_parameters = Eval.me(sample_swap_tool_parameters)
    if( sample_swap_tool =~ /somalier/ ) {
      somalier_extract_parameters = sample_swap_tool_parameters['somalier_extract'] ? tx_quant_tool_parameters['somalier_extract'] : ''
      somalier_relate_parameters = sample_swap_tool_parameters['somalier_relate'] ? tx_quant_tool_parameters['somalier_relate'] : ''
      somalier_extract(
        bams,
        dna_ref,
        known_sites,
        somalier_extract_parameters)
      somalier_extract.out.somalier_extracts
        .map{ [it[0], it[2], it[3]] }
        .groupTuple(by: [0, 1])
        .set{ somalier_relate_inputs }
      somalier_relate(
        somalier_relate_inputs,
        somalier_relate_parameters)
    }
}

